class CommentsController < ApplicationController

  #http_basic_authenticate_with :name => "root", :password => "root", :only => :destroy

  def create
    @patient = Patient.find(params[:patient_id])
    @comment = @patient.comments.create(params[:comment])
    redirect_to patient_path(@patient)
  end

  def destroy
    @patient = Patient.find(params[:patient_id])
    @comment = @patient.comments.find(params[:id])
    @comment.destroy
    redirect_to patient_path(@patient)
  end
end

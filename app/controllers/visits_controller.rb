class VisitsController < ApplicationController

  # GET /visits
  # GET /visits.json
  def index
    magicParam = params[:magic]
    getSinglePatient = params[:getSinglePatient]
    sortBy = ""
    sortByParam = 3
    if params[:sortBy]
      sortByParam = Integer(params[:sortBy])
    end
    sortByDirection = "DESC"
    if params[:sortByDirection]
      sortByDirection = (Integer(params[:sortByDirection]) == 0 ? "DESC" : "ASC")
    end
    case sortByParam % 4
      when 0
        #"JMBG"
        sortBy = "patients.jmbg " + sortByDirection
      when 1
        #"Patient"
        sortBy = "patients.name " + sortByDirection
      when 2
        #"Technician"
        sortBy = "technicians.name " + sortByDirection
      when 3
        #"Date"
        sortBy = "visits.date " + sortByDirection
    end
    
    if magicParam
      @visits = Visit.joins(:patient, :technician)\
        .where('patients.jmbg LIKE ? OR patients.name LIKE ? OR visits.date LIKE ? OR technicians.name LIKE ?',\
            "%#{params[:magic]}%", "%#{params[:magic]}%", "%#{params[:magic]}%", "%#{params[:magic]}%")\
        .order(sortBy)
    else
      @visits = Visit.joins(:patient, :technician).order(sortBy)
    end

    if @visits.length == 0 and getSinglePatient == "1"
      potentialPatients = Patient.where("patients.name LIKE ?", "%#{params[:magic].gsub("*", "%")}%")
      if potentialPatients.length == 1
        @patient = potentialPatients[0]
      else
        @patient = {}
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => {:visits => @visits.to_json(:include => [:patient, :technician]), :patient => @patient} }
    end
  end

  # GET /visits/1
  # GET /visits/1.json
  def show
    @visit = Visit.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @visit }
    end
  end

  # GET /visits/new
  # GET /visits/new.json
  def new
    @visit = Visit.new
    @patients = Patient.all
    @technicians = Technician.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @visit }
    end
  end

  # GET /visits/1/edit
  def edit
    @visit = Visit.find(params[:id])
    @patients = Patient.all
    @technicians = Technician.all
  end

  # POST /visits
  # POST /visits.json
  def create
    visitParams = params[:visit]
    patients = []
    if params[:patient]
      patients = Patient.where('patients.jmbg=?', "#{params[:patient][:jmbg]}")
    end

    if patients.length == 0
      puts("patients.length == 0")
      patient = Patient.new(params[:patient])
    elsif patients.length == 1
      patient = patients[0]
      puts("patients.length == 1 ")
      puts(patient[:id])
    end
    if params[:patient]
      visitParams[:patient] = patient
    end
    @visit = Visit.new(visitParams)

    respond_to do |format|
      if @visit.save
        format.html { redirect_to @visit, notice: 'Visit was successfully created.' }
        format.json { render json: @visit, status: :created, location: @visit }
      else
        format.html { render action: "new" }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /visits/1
  # PUT /visits/1.json
  def update
    @visit = Visit.find(params[:id])

    respond_to do |format|
      if @visit.update_attributes(params[:visit])
        format.html { redirect_to @visit, notice: 'Visit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /visits/1
  # DELETE /visits/1.json
  def destroy
    @visit = Visit.find(params[:id])
    @visit.destroy

    respond_to do |format|
      format.html { redirect_to visits_url }
      format.json { head :no_content }
    end
  end
end
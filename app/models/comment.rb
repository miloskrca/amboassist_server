class Comment < ActiveRecord::Base
  belongs_to :patient
  attr_accessible :body, :commenter
end

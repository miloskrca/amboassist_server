class Technician < ActiveRecord::Base
  attr_accessible :jmbg, :name, :dob, :sex

  validates :name, :presence => true
  validates :jmbg, :presence => true
  validates :dob, :presence => true

  #has_many :comments, :dependent => :destroy
  has_many :visits
end

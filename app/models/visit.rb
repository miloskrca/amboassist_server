class Visit < ActiveRecord::Base
  attr_accessible :date, :diagnose, :patient_id, :patient, :symptoms, :technician_id, :technician, :address

  belongs_to :patient
  belongs_to :technician
end

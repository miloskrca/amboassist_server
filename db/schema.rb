# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130917193516) do

  create_table "comments", :force => true do |t|
    t.string   "commenter"
    t.text     "body"
    t.integer  "patient_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "comments", ["patient_id"], :name => "index_comments_on_patient_id"

  create_table "patients", :force => true do |t|
    t.string   "name"
    t.string   "jmbg"
    t.date     "dob"
    t.boolean  "sex"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "technicians", :force => true do |t|
    t.string   "name"
    t.string   "jmbg"
    t.date     "dob"
    t.boolean  "sex"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "visits", :force => true do |t|
    t.string   "address"
    t.string   "diagnose"
    t.string   "symptoms"
    t.datetime "date"
    t.integer  "patient_id"
    t.integer  "technician_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "visits", ["patient_id"], :name => "index_visits_on_patient_id"
  add_index "visits", ["technician_id"], :name => "index_visits_on_technician_id"

end

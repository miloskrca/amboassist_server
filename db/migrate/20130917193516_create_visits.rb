class CreateVisits < ActiveRecord::Migration
  def change
    drop_table :visits
    create_table :visits do |t|
      t.string :address
      t.string :diagnose
      t.string :symptoms
      t.datetime :date
      t.references :patient
      t.references :technician

      t.timestamps
    end
    add_index :visits, :patient_id
    add_index :visits, :technician_id
  end
end

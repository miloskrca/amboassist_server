class CreatePatients < ActiveRecord::Migration
  def change
    drop_table :patients
    create_table :patients do |t|
      t.string :name
      t.string :jmbg
      t.date :dob
      t.boolean :sex

      t.timestamps
    end
  end
end

class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :commenter
      t.text :body
      t.references :patient

      t.timestamps
    end
    add_index :comments, :patient_id
  end
end

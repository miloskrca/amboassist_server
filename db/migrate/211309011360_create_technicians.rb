class CreateTechnicians < ActiveRecord::Migration
  def change
    drop_table :technicians
    create_table :technicians do |t|
      t.string :name
      t.string :jmbg
      t.date :dob
      t.boolean :sex

      t.timestamps
    end
  end
end
